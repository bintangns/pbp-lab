from django.urls import path
from .views import index, friend_list

urlpatterns = [
    path('', index, name='index'), #refer ke function index ke views
    path('friends', friend_list, name='Friend List'), #refer ke friendlist di views
    # TODO Add friends path using friend_list Views
]
