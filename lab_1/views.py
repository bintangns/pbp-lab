from django.shortcuts import render
from datetime import datetime, date
from .models import Friend

mhs_name = 'Bintang Nursyawalli Sidi'  # TODO Implement this// ini response
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2001, 12, 20)  # TODO Implement this, format (Year, Month, Date)
npm = 2006597790  # TODO Implement this


def index(request): #nunjukin yang di url
    response = {'name': mhs_name,
                'age': calculate_age(birth_date.year),
                'npm': npm}
    return render(request, 'index_lab1.html', response) #nunjuk ke index_1 html datanya ada di response


def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0


def friend_list(request):
    friends = Friend.objects.all().values()  # TODO Implement this
    response = {'friends': friends} #buat nunjukin friends di urls objek friend ada di model disimpen di db.sqlite3(database)
    return render(request, 'friend_list_lab1.html', response)
