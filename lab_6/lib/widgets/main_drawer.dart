import 'package:flutter/material.dart';

class MainDrawer extends StatelessWidget {
  Widget buildListTile(String title, IconData icon, void Function() tapHandler) {
    return ListTile(
      leading: Icon(
        icon,
        size: 26,
      ),
      title: Text(
        title,
        style: TextStyle(
          fontFamily: 'RobotoCondensed',
          fontSize: 24,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: tapHandler,
    );
  }

@override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            height: 120,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            alignment: Alignment.centerLeft,
            color: Color.fromARGB(255,23,30,36),
            child: Text(
              'Ngingetin',
              style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 30,
                  color: Colors.white
                  ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          buildListTile('Home', Icons.home,  () {
            Navigator.pop(context);
             }),
            buildListTile('Filter', Icons.settings, () {
            Navigator.pop(context);
            }),
          buildListTile('Buat Jadwal', Icons.calendar_today_rounded, () {
            Navigator.pop(context);
             }),
          buildListTile('Reminder', Icons.calendar_today_rounded, () {
            Navigator.pop(context);
             }),
          buildListTile('View List', Icons.calendar_today_rounded, () {
            Navigator.pop(context);
             }),
          buildListTile('Content', Icons.calendar_today_rounded, () {
            Navigator.pop(context);
            
          }),

        ],
      ),
    );
  }
}
