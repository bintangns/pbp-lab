1. Apakah perbedaan antara JSON dan XML?
2. Apakah perbedaan antara HTML dan XML?

1. Perbedaan antara JSON dan XML adalah
Bahasa : XML : XML merupakan bahasa markup dan bukanlah bahasa pemrograman yang memiliki tag untuk mendefinisikan elemen.
         JSON : JSON hanyalah format yang ditulis dalam JavaScript

Kecepatan : XML : Besar dan lambat dalam penguraian, menyebabkan transmisi data lebih lambat.
            JSON : Sangat cepat karena ukuran file sangat kecil dan penguraian lebih cepat oleh mesin JavaScript

Penggunaan Array : XML : XML tidak mensupport array secara langsung, jika ingin menggunakan array harus menggunakan tag.
                   JSON : Mensupport Array.


2. Perbedaan HTML dan XML adalah
- XML berfokus pada transfer data sedangkan HTML untuk penyajian data
- XML itu case sensitive sedangkan HTML tidak.
- Tag XML dapat dikembangkan sedangkan HTML memiliki Tag terbatas.
- XML adalah singkatan dari eXtensible Markup Language sedangkan HTML adalah singkatan dari Hypertext Markup Language.