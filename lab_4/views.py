from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm
from django.http.response import HttpResponse, HttpResponseRedirect

# Create your views here.
def index(request):
    Notes = Note.objects.all().values()  
    response = {'Notes': Notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    context={}
    form = NoteForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect('/lab-4')

    context['form']= form
    return render(request, "lab4_form.html", context)

def note_list(request):
    Notes = Note.objects.all()
    response = {'Notes' : Notes}
    return render(request, 'lab4_note_list.html' , response)
