import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '/widgets/main_drawer.dart';
import 'package:google_fonts/google_fonts.dart';
// import 'home_screen.dart';
// import 'events_screen.dart';

import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class TabsScreen extends StatefulWidget {
  static const routeName = '/tabs_screen';

  const TabsScreen({Key? key}) : super(key: key);

  @override
  State<TabsScreen> createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {

  final PersistentTabController? _controller =
      PersistentTabController(initialIndex: 0);
  final _formKey = GlobalKey<FormState>();
  void _processData(){
    _formKey.currentState?.reset();
  }

  // List<Widget> _buildScreens() {
  //   return [const HomeScreen(), const EventsScreen()];
  // }


  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Ngingetin'),
        backgroundColor: Color.fromARGB(255,23,30,36),
      ),
      drawer: MainDrawer(),
      body:SingleChildScrollView(
        child: Column(children: <Widget>[
          Container(
            height: 150,
            width: 500,
            child: Stack(children: <Widget>[
              Container(
                padding: EdgeInsets.only(left: 30, right : 30),
                height: 200,
              decoration: BoxDecoration(color: Color.fromARGB(255,23,30,36),
              borderRadius: BorderRadius.only(

              )
              ),
              
            child: Row(children: <Widget>[
              Text('About', 
              style: GoogleFonts.dmSans(fontSize: 40, fontWeight: FontWeight.bold, color: Color.fromRGBO(255,255,255,1)),),
              Spacer(),
              // Image.asset("../images/clock2.png",
              // height: 30,
              // width: 30,)
              ],),)

            ],),
          
          ),
          Container(
            padding: EdgeInsets.only(left: 30, right : 30),
            height: 800,
            width: 500,
            
            child: Column(children: <Widget>[
              Text(" "),
               Text('     Dimasa pandemi Covid-19 ini banyak orang yang harus terpaksa melakukan WFH dan juga bersekolah secara online, banyak dari mereka yang membutuhkan suatu website yang berfungsi untuk mengatur dan mengingatkan akan jadwal dan tugas mereka. Dengan Ngingetin, maka semua kebutuhan dalam mengatur jadwal akan terpenuhi.'
               , 
              style: GoogleFonts.dmSans(fontSize: 16,  color: Color.fromRGBO(255,255,255,1)),),
              Text('Aplikasi yang diajukan adalah aplikasi yang berfungsi sebagai pengatur kegiatan (project management) yang menawarkan fitur-fitur penting dalam mengerjakan sebuah proyek tertentu, baik individu maupun kelompok yang nantinya gabungan informasi dapat dilihat atau di-tracking.', 
              style: GoogleFonts.dmSans(fontSize: 16, color: Color.fromRGBO(255,255,255,1)),),
              // Text('Created By :', textAlign: TextAlign.left ,style: GoogleFonts.dmSans(fontSize: 16, color: Color.fromRGBO(255,255,255,1),),),
              Text(' '),

              Form(
                key: _formKey,
                  child: Container(child: Column(children: [
                TextFormField(
                  decoration: InputDecoration(hintText: "Your Name",
                      labelText: "Nama",
                      labelStyle: TextStyle(fontSize: 25, color: Colors.white),
                      border: OutlineInputBorder(),

                      hintStyle: TextStyle(color: Colors.white)),
                  style: TextStyle(color: Colors.white),
                  maxLines: 1,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Nama tidak boleh kosong';
                    }
                    return null;
                  },


                ),
                TextFormField(
                  decoration: InputDecoration(hintText: "Pesan",
                      labelText: "Pesan",
                      labelStyle: TextStyle(fontSize: 25, color: Colors.white), floatingLabelStyle: TextStyle(fontSize: 20, color: Colors.white),
                      border: OutlineInputBorder(),
                      hintStyle: TextStyle(color: Colors.white)),
                  style: TextStyle(color: Colors.white),
                  maxLines: 6,
                  
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Pesan tidak boleh kosong';
                    }
                    return null;
                  },
                ),
                Text(' '),
                RaisedButton(
                  child: Text(
                    "Submit",
                    style: TextStyle(color: Colors.black),
                  ),
                  color: Colors.white,
                  onPressed: () {
                    if (_formKey.currentState!.validate()){
                      return (_formKey.currentState?.reset());
                      print(_formKey);
                    }
                    
                    ;
      
                  }
                ),
              ],)))

            ],),
            
            decoration: BoxDecoration(color: Color.fromARGB(255,23,30,36),
            
          ),

          ),


          
        ],),
      )
      // body:
      // Center(
      //   child: SingleChildScrollView(child: Column(children: <Widget>[
      //     Container(child: Text('NGINGETIN', style: GoogleFonts.dmSans(fontSize: 40, fontWeight: FontWeight.bold),),color : Color.fromARGB(2, 2, 2, 2), padding: EdgeInsets.all(200.0)),
          
      //   Container(
      //       child: Text('Ini Container'),color: Color.fromARGB(255, 28, 42, 53), padding: EdgeInsets.all(4000.0),
      //     ),
      //   Text('Ngingetin App', 
      //   style: GoogleFonts.dmSans(
      //     fontSize: 40, 
      //     fontWeight: FontWeight.bold,)
      //     ,)
      //   ]
      //   ,)
      //   ,)
      //   ,),
    );
  }
}